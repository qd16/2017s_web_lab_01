## Herbivorous
* Horse
* Cow
* Sheep

## Canivorous
* Cat
* Dog
* Polar Bear
* Hawk
* Lion
* Fennec Fox

## Omnivorous
* Frog
* Rat
* Fox
* Raccoon
* Chicken
